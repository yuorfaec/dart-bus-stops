# DART Bus Stops

Comparing the data that Dallas Area Rapid Transit (DART) provides to Google vs where the bus stops actually are.

Started off in Plano after noticing many issues on the 236 and 241. Most of the data was obtained by driving the bus routes in a private vehicle and using a GPS tracking app to note where each stop is actually located. 

# Resources
1. [Google Map summarizing issues in Plano](https://www.google.com/maps/d/viewer?mid=1dyV9SsXAjxkEBCqayVLSxXljAEx3BjKr&ll=33.032570314147954%2C-96.79371599999999&z=12)
2. [CSV summary of stops with issues in Plano](/DART_bus_stops_summary.csv)
3. [Google Earth file with raw data](/DART_Bus_Stops_all.kmz)

# Google Earth

The [KMZ file](/DART_Bus_Stops_all.kmz) with the raw data can be opened in Google Earth Pro (desktop), Google Earth Web (desktop browser), or the Google Earth App (mobile).

## Google Earth Pro (desktop)
Even though it is called 'Pro', it is still completely free.

1. Download [Google Earth Pro](https://www.google.com/earth/versions/#download-pro) for your OS
2. Download the [KMZ file](/DART_Bus_Stops_all.kmz) to an easily-retrievable location
3. Click 'File'->'Open' and select the [KMZ file](/DART_Bus_Stops_all.kmz)
4. Under the 'Places' heading on the left, check and uncheck the folders based off the desired view

## Google Earth Web (desktop browser)
1. Download the [KMZ file](/DART_Bus_Stops_all.kmz) to an easily-retrievable location
2. Go to [Google Earth web](https://earth.google.com/web/)
3. Click the three lines at the top left to expand the options menu
4. Click 'Projects'->'Open'->'Import KML file from computer' and select the [KMZ file](/DART_Bus_Stops_all.kmz)
5. Hide and show the layers by hovering over the folder name and clicking the eye icon to the right

## Google Earth App (mobile)
1. Download [Google Earth](https://www.google.com/earth/versions/#earth-for-mobile) for Android or Apple
2. Download the [KMZ file](/DART_Bus_Stops_all.kmz) to an easily-retrievable location
3. Tap the three lines at the top left to expand the options menu
4. Tap 'Projects'->'Open'->'Import KML file' and select the [KMZ file](/DART_Bus_Stops_all.kmz)
5. Hide and show the layers by swiping up from the bottom of the screen, tapping the three dots to the right of the folder name, and tapping 'Hide feature' or 'Show feature'

# Roadmap
Open to including more DART stops based off need and interest. Perhaps we can create a tool for people to submit their own KML/KMZ GPS tracks? Generate new GTFS files with suggested changes?

## To-do
- Document process to export summary maps
    - Figure out a way to either extract CSV from KMZ/KML or automate the formatting of the exports from My Maps
- Determine how to handle GTFS file updates from DART (already out of date according to [OpenMobilityData](https://transitfeeds.com/p/dart/26?p=1))
- Determine which stops are within the margin of error for GPS

# Contributing
Open to contributions! Open a pull request or an issue. 

Note that Google My Maps has strict limits on data. No more than 7 layers and no more than 2000 items per layer. Summary maps (like the map [prepared for Plano](https://www.google.com/maps/d/viewer?mid=1dyV9SsXAjxkEBCqayVLSxXljAEx3BjKr&ll=33.032570314147954%2C-96.79371599999999&z=12)) will need to be broken up by city or region.

DART's latest coordinates provided to Google/GoPass can be found in the stops.txt file here: http://www.dart.org/transitdata/latest/google_transit.zip

# Authors and acknowledgment
Thanks to [OpenMobilityData](https://transitfeeds.com/) for linking to DART's GTFS files and to the contributors of [pandas](https://pandas.pydata.org/) for making it easy to analyze these files.
